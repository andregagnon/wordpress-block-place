<?php
/**
 * Plugin Name:     Block for Place custom post type
 * Plugin URI:      https://greenlandnhhistory.org
 * Description:     Query loop block for historic places.
 * Author:          Andre Gagnon
 * Author URI:      https://andregagnon.com
 * Text Domain:     block-place
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Block_Place
 */

define ( 'BLOCK_PLACE_DIR', plugin_dir_path( __FILE__ ) );

// custom post type
require 'taxonomies/era.php';
require 'post-types/place.php';

// block
// Register Custom Blocks
add_action('acf/init', 'block_place_register_blocks');
function block_place_register_blocks() {

    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a places block.
        acf_register_block_type(array(
            'name'				=> 'places',
            'title'				=> __( 'Places'),
            'description'		=> __( 'Display 1 or more Places.'),
            'render_template' => dirname( __file__ ) . '/blocks/places/block.php',
            'category'			=> 'common',
            'icon'				=> 'location',
            'keywords'			=> array( 'place' ),
            'enqueue_style'   => plugin_dir_url( __FILE__ ) . '/blocks/places/place.css',
        ));


        // register a place block.
        acf_register_block_type(array(
            'name'				=> 'place',
            'title'				=> __( 'Place'),
            'description'		=> __( 'Display 1 place.'),
            'render_template' => dirname( __file__ ) . '/blocks/place/block.php',
            'category'			=> 'common',
            'icon'				=> 'location-alt',
            'keywords'			=> array( 'place' ),
            'enqueue_style'   => plugin_dir_url( __FILE__ ) . '/blocks/place/place.css',
        ));


    }
}
