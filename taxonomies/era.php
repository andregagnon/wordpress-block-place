<?php

/**
 * Registers the `era` taxonomy,
 * for use with 'place'.
 */
function era_init() {
	register_taxonomy( 'era', [ 'place' ], [
		'hierarchical'          => false,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => false,
		'query_var'             => true,
		'rewrite'               => true,
		'capabilities'          => [
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		],
		'labels'                => [
			'name'                       => __( 'Eras', 'block-place' ),
			'singular_name'              => _x( 'Era', 'taxonomy general name', 'block-place' ),
			'search_items'               => __( 'Search Eras', 'block-place' ),
			'popular_items'              => __( 'Popular Eras', 'block-place' ),
			'all_items'                  => __( 'All Eras', 'block-place' ),
			'parent_item'                => __( 'Parent Era', 'block-place' ),
			'parent_item_colon'          => __( 'Parent Era:', 'block-place' ),
			'edit_item'                  => __( 'Edit Era', 'block-place' ),
			'update_item'                => __( 'Update Era', 'block-place' ),
			'view_item'                  => __( 'View Era', 'block-place' ),
			'add_new_item'               => __( 'Add New Era', 'block-place' ),
			'new_item_name'              => __( 'New Era', 'block-place' ),
			'separate_items_with_commas' => __( 'Separate Eras with commas', 'block-place' ),
			'add_or_remove_items'        => __( 'Add or remove Eras', 'block-place' ),
			'choose_from_most_used'      => __( 'Choose from the most used Eras', 'block-place' ),
			'not_found'                  => __( 'No Eras found.', 'block-place' ),
			'no_terms'                   => __( 'No Eras', 'block-place' ),
			'menu_name'                  => __( 'Eras', 'block-place' ),
			'items_list_navigation'      => __( 'Eras list navigation', 'block-place' ),
			'items_list'                 => __( 'Eras list', 'block-place' ),
			'most_used'                  => _x( 'Most Used', 'era', 'block-place' ),
			'back_to_items'              => __( '&larr; Back to Eras', 'block-place' ),
		],
		'show_in_rest'          => true,
		'rest_base'             => 'era',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	] );

}

add_action( 'init', 'era_init' );

/**
 * Sets the post updated messages for the `era` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `era` taxonomy.
 */
function era_updated_messages( $messages ) {

	$messages['era'] = [
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Era added.', 'block-place' ),
		2 => __( 'Era deleted.', 'block-place' ),
		3 => __( 'Era updated.', 'block-place' ),
		4 => __( 'Era not added.', 'block-place' ),
		5 => __( 'Era not updated.', 'block-place' ),
		6 => __( 'Eras deleted.', 'block-place' ),
	];

	return $messages;
}

add_filter( 'term_updated_messages', 'era_updated_messages' );
