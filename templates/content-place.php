<?php
/**
 * Block Place content template.
 *
 * This is the template that displays the place.
 */
?>

      <?php $image = get_field('image', get_the_ID()); ?>
      <?php if ($image && $image != "null"): ?>
        <?php $image = "/wp-content/uploads/".$image; ?>
        <!-- <?php echo $image; ?> -->
      <?php endif; ?>
      <?php $index = get_field('index', get_the_ID()); ?>

      <div class="place">
        <div class="wp-block-media-text has-media-on-the-left is-stacked-on-mobile is-vertically-aligned-top">
          <figure class="wp-block-media-text__media">
            <?php if ($image && $image != "null"): ?>
              <img loading="lazy" src="<?php echo $image; ?>" />
              <!-- TBD add alt tag -->
            <p></p>

            <?php endif; ?>
          </figure>
          <div class="wp-block-media-text__content">
            <!-- <h2>Heading</h2> -->
            <h3><?php the_title(); ?></h3>
            <strong><?php the_field('year', get_the_ID()); ?></strong><br />
            <span><?php the_field('address', get_the_ID()); ?></span><br />

            <?php $long_description = get_field('long_description', get_the_ID()); ?>
            <?php if ( $long_description ): ?>
              <p><?php the_field('long_description', get_the_ID()); ?></p><br />
            <?php else: ?>
              <?php the_content(); ?>
            <?php endif; ?>

            <?php if ( $index && $index != "null" && $index != '00'): ?>
            	<small><a href="/history/maps">Map</a> ID: <?php echo $index; ?></small><br />
						<?php endif; ?>
            <p></p>
          </div>
        </div>
      </div>
