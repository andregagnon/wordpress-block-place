=== Block for Places ===
Contributors: Andre Gagnon
Donate link:
Tags: custom-block
Requires at least: 4.5
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Custom ACF block for post-type 'place'.

== Description ==

Using a plugin, create custom post type, taxonomy, blocks, and a template.

== Installation ==

1. Upload `block-place.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Require Advance Custom Fields plugin.

== Frequently Asked Questions ==


== Changelog ==
