<?php
/**
 * Block Name: Places
 *
 * This is the template that displays the places loop block.
 */


  $place = get_field('select');
  // echo "place:".print_r( $place );
  // echo "<br />";

  $args = array(
    'orderby' => 'post_name',
    'order' => 'ASC',
    'post_type' => 'place',
    'p' => $place
  );


// print_r( $args );

$the_query = new WP_Query($args);
?>

<?php
  if ($the_query->have_posts()) :
    while ($the_query->have_posts()) : $the_query->the_post(); ?>

      <?php $image = get_field('image', get_the_ID()); ?>
      <?php if ($image && $image != "null"): ?>
        <?php $image = "/wp-content/uploads/".$image; ?>
        <!-- <?php echo $image; ?> -->
      <?php endif; ?>


      <!-- replace template, factor? -->
      <?php include BLOCK_PLACE_DIR . 'templates/content-place.php'; ?>


    <?php endwhile; ?>
<?php endif;?>
