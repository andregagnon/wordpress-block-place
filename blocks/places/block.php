<?php
/**
 * Block Name: Places
 *
 * This is the template that displays the places loop block.
 */

$argType = get_field('loop_type_argument');

if ($argType == "count") :
  $args = array(
    'orderby' => 'post_title',
    'order' => 'ASC',
    'post_type' => 'place',
    'posts_per_page' => get_field('count')
  );
else:
  $places = get_field('select');
  $args = array(
    'orderby' => 'post_name',
    'order' => 'ASC',
    'post_type' => 'place',
    'post__in' => $places
  );
endif;

// print_r( $args );

$the_query = new WP_Query($args);
?>

<?php
  if ($the_query->have_posts()) :
    while ($the_query->have_posts()) : $the_query->the_post(); ?>

      <!-- replace template, factor? -->
      <?php include BLOCK_PLACE_DIR . 'templates/content-place.php'; ?>

      <hr style="background-color:#ccc;"/>

    <?php endwhile; ?>
<?php endif;?>
