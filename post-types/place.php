<?php

/**
 * Registers the `place` post type.
 */
function place_init() {
	register_post_type(
		'place',
		[
			'labels'                => [
				'name'                  => __( 'Places', 'block-place' ),
				'singular_name'         => __( 'Place', 'block-place' ),
				'all_items'             => __( 'All Places', 'block-place' ),
				'archives'              => __( 'Place Archives', 'block-place' ),
				'attributes'            => __( 'Place Attributes', 'block-place' ),
				'insert_into_item'      => __( 'Insert into Place', 'block-place' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Place', 'block-place' ),
				'featured_image'        => _x( 'Featured Image', 'place', 'block-place' ),
				'set_featured_image'    => _x( 'Set featured image', 'place', 'block-place' ),
				'remove_featured_image' => _x( 'Remove featured image', 'place', 'block-place' ),
				'use_featured_image'    => _x( 'Use as featured image', 'place', 'block-place' ),
				'filter_items_list'     => __( 'Filter Places list', 'block-place' ),
				'items_list_navigation' => __( 'Places list navigation', 'block-place' ),
				'items_list'            => __( 'Places list', 'block-place' ),
				'new_item'              => __( 'New Place', 'block-place' ),
				'add_new'               => __( 'Add New', 'block-place' ),
				'add_new_item'          => __( 'Add New Place', 'block-place' ),
				'edit_item'             => __( 'Edit Place', 'block-place' ),
				'view_item'             => __( 'View Place', 'block-place' ),
				'view_items'            => __( 'View Places', 'block-place' ),
				'search_items'          => __( 'Search Places', 'block-place' ),
				'not_found'             => __( 'No Places found', 'block-place' ),
				'not_found_in_trash'    => __( 'No Places found in trash', 'block-place' ),
				'parent_item_colon'     => __( 'Parent Place:', 'block-place' ),
				'menu_name'             => __( 'Places', 'block-place' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title', 'editor' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => false,
			'rest_base'             => 'place',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'taxonomies'						=> [ 'post_tag' ],

		]
	);

}

add_action( 'init', 'place_init' );

/**
 * Sets the post updated messages for the `place` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `place` post type.
 */
function place_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['place'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Place updated. <a target="_blank" href="%s">View Place</a>', 'block-place' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'block-place' ),
		3  => __( 'Custom field deleted.', 'block-place' ),
		4  => __( 'Place updated.', 'block-place' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Place restored to revision from %s', 'block-place' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Place published. <a href="%s">View Place</a>', 'block-place' ), esc_url( $permalink ) ),
		7  => __( 'Place saved.', 'block-place' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Place submitted. <a target="_blank" href="%s">Preview Place</a>', 'block-place' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Place scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Place</a>', 'block-place' ), date_i18n( __( 'M j, Y @ G:i', 'block-place' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Place draft updated. <a target="_blank" href="%s">Preview Place</a>', 'block-place' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'place_updated_messages' );

/**
 * Sets the bulk post updated messages for the `place` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `place` post type.
 */
function place_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['place'] = [
		/* translators: %s: Number of Places. */
		'updated'   => _n( '%s Place updated.', '%s Places updated.', $bulk_counts['updated'], 'block-place' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Place not updated, somebody is editing it.', 'block-place' ) :
						/* translators: %s: Number of Places. */
						_n( '%s Place not updated, somebody is editing it.', '%s Places not updated, somebody is editing them.', $bulk_counts['locked'], 'block-place' ),
		/* translators: %s: Number of Places. */
		'deleted'   => _n( '%s Place permanently deleted.', '%s Places permanently deleted.', $bulk_counts['deleted'], 'block-place' ),
		/* translators: %s: Number of Places. */
		'trashed'   => _n( '%s Place moved to the Trash.', '%s Places moved to the Trash.', $bulk_counts['trashed'], 'block-place' ),
		/* translators: %s: Number of Places. */
		'untrashed' => _n( '%s Place restored from the Trash.', '%s Places restored from the Trash.', $bulk_counts['untrashed'], 'block-place' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'place_bulk_updated_messages', 10, 2 );
